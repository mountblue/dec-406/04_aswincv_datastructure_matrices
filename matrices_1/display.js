/*
Name of the project : Datastructure_matrices
File name : matrices_1/display.js
Description : to check wheather a given track for the train goes beyond limit.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : 2,3,"LLRU"
Output : possible
*/


function find(m,n,path) {
    var flag = false;
    for (var i = 0; i < m; i++) {
        for (var j = 0; j < n; j++) {
          if( check(i,j,m,n,path) ) {
            console.log("possible");
            flag=true;
            break;
          }
        }
    }
    if(!flag) {
        console.log("not possible"); }
}


function check( x, y, m, n, path) {
      for (var i = 0; i < path.length ; i++) {
        switch( path.charAt(i) ) {
            case 'U' : x--;
                break;
            case 'D' : x++;
                break;
            case 'L' : y--;
                break;
            case 'R' : y++
        }
      }
      if(x >= m || x<0 || y>=n || y<0 ){
        return false;
      }else {
        return true;
      }
}


find(2,3,"LLRU");
