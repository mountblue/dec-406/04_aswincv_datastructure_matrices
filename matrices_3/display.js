/*
Name of the project : Datastructure_matrices
File name : matrices_3/display.js
Description : to check the destination can be reached from the source.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [[0,3,1,0],[3,0,3,3],[2,3,0,3],[0,3,3,3]],4,4
Output : yes
*/


var flag = false ;

function find(arr,m,n) {
    var posx,posy;
    //searching the position of source in the input
    for (var i = 0; i < arr.length; i++) {
      for (var j = 0; j < arr.length; j++) {
          if(arr[i][j] == 1) {
              posx = i;
              posy = j;
              break;
          }
      }
    }
    search(arr,posx,posy,m,n);
    if(flag) {
        console.log("yes");
    }else {
        console.log("no");
    }
}


//function for movement from one point to the other. which is called recurssively
function search(arr,x,y,m,n) {
    if(!flag) {
        if(arr[x+1][y]==2&&(x+1<m)) {
            flag = true;
            return;
        }else if (arr[x][y+1]==2&&(y+1)<n) {
            flag = true;
            return;
        }else if(arr[x][y-11]==2&&(y-11)>-1) {
            flag =true;
            return;
        }
        if(x!=0) {
            search(arr,x-1,y,m,n);
        }
        if(x!=m-1) {
            search(arr,x+1,y,m,n);
        }
        if(y!=0) {
            search(arr,x,y-1,m,n);
        }
        if(x!=m-1) {
            search(arr,x,y+1,m,n);
        }
     }else {
        return;
     }
}


find([[0,3,1,0],[3,0,3,3],[2,3,0,3],[0,3,3,3]],4,4);
